# Gestion de version avec GIT

Un petit tutoriel vidéo pour les novices (en français):

https://www.youtube.com/watch?v=hPfgekYUKgk

## Installation du client GIT

Téléchargez le ici: https://git-scm.com/download/win

Laissez les options par défaut.

## Git workflow

Nom de la branche | Description
------------- | -------------
master | code se trouvant sur le serveur
develop | branche de développement
feature/... | branche ajout de fonctionalité
fix/... | branche de correction de fonctionalité
doc/ | branche de documentation (mais c'est mieux de faire la doc en même temps que le développement)

* Configurer git pour le repo actuel :
```sh
git config user.name "John Doe"
git config user.email johndoe@example.com
```

* Créer des alias pour aller plus vite :
```sh
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.ci commit
git config --global alias.st status
git config --global alias.cp cherry-pick
```

* Créer une nouvelle branche locale et se placer dessus :
```sh
git checkout -b <newBranchName>
```
> A noter : avec la commande ci-dessus, la branche créé sera basé sur la branche sur laquel on est placé.
> On peut se baser sur une branche spécifique peu importe ou l'on se trouve en utilisant la commande ci-dessous :

```sh
git checkout -b <newBranchName> <fromBranchName>
```

* Récuperer une branche distante :
```sh
git checkout -b <localBranchName> <remoteBrancheName>
```
> Exemple : git checkout -b feature/09/emailing origin/feature/09/emailing

* Annuler les modifs d'un fichier (le fichier ne doit pas être ajouté au commit) :
```sh
git checkout <file>
```

* Ajouter un fichier|dossier au prochain commit :
```sh
git add <file|folder>
```

* Ajouter tous les fichiers modifiés, nouveaux et supprimés pour le prochain commit :
```sh
git add -A
```

* Annuler l'ajout d'un fichier :
```sh
git reset <file>
```

* Créer un nouveau commit :
```sh
git commit -m 'New commit message'
```

* Annuler le dernier commit :
```sh
git reset HEAD^
```

* Annuler les x derniers commit :
```sh
git reset HEAD~x
```

* Pousser une branche locale sur le repo distant :
```sh
git push origin <branchName>
```

* Supprimer une branche locale
```sh
git branch -d <branchName>
```

* Clean son cache (utile quand on modifie le .gitignore)
```
git rm -r --cached .
```
